## The Complex Path for Wireless in the Manufacturing Industry

### Abstract
The manufacturing industry is at the edge of the 4th industrial revolution, 
a paradigm of integrated architectures in which the entire production chain 
(composed of machines, workers and products) is intrinsically connected. 
Wireless technologies can become pivotal to enable this manufacturing revolution.
However, we identify some signs that indicate that wireless could
be left out from the next generation of smart-factory equipment.
This problem is particularly relevant considering that the heavy
machinery used in this sector can last for decades. 
We argue that at the core of this issue there is a mismatch between industrial needs 
and the interests of academic and partly-academic (such as standardization bodies) sectors. 
We base our claims on surveys from renowned advisory firms and interviews with industrial actors, 
which we contrast with results from content analysis of scientific articles. 
Finally we propose some convergence paths that, 
while still retaining the degree of novelty required for academic purposes, 
are more aligned with industrial concerns.

### Dataset
The corpus, obtained through IEEEXplore, consists of all the articles published between 2015 and 2017 (both included) 
in the following IEEE journals:

* Communication Letters
* Communications Magazine
* Sensors Journal
* Transactions on Industrial Informatics
* Transactions on Mobile Computing
* Transactions on Wireless Communications
* Wireless Communication Letters

as well as IEEE conferences (including their corresponding workshops):

* Globecom
* ICC
* Infocom
* Sensors
* WCNC

### Source
Python Jupyter Notebooks.